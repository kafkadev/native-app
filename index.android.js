'use strict';
import { AppRegistry } from 'react-native';
import reduxBaseLink from './src/index.js';

AppRegistry.registerComponent('reduxBaseLink', () => reduxBaseLink);
