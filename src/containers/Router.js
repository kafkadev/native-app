import React from 'react';
import { Scene, Router, ActionConst, Actions, Reducer, Route } from 'react-native-router-flux';
import PageOne from '../components/PageOne';
import PageTwo from '../components/PageTwo';
import Home from '../components/Home'
import Donemec from './donemec'
import DemoContentRoute from './DemoContentRoute'
const PageOneC = connect()(PageOne);
const PageTwoC = connect()(PageTwo);
const DonemecC = connect()(Donemec);
const RouterWithRedux = connect()(Router);
import { connect, Provider } from 'react-redux';
const reducerCreate = params=>{
    const defaultReducer = Reducer(params);
    return (state, action)=>{
        console.log("ACTION:", action,state);
        return defaultReducer(state, action);
    }
};

class TabIcon extends React.Component {
    render(){
        return (
            <Text style={{color: this.props.selected ? 'red' :'black'}}>{this.props.title}</Text>
        );
    }
}



const Scenes = Actions.create(
                <Scene key="root">
                    <Scene
                        key="pageOne"
                        component={PageOneC}
                        title="Page BIR"
                    />
                    <Scene
                        key="pageTwo"
                        component={PageTwoC}
                        title="Page Sonra"
                        rightTitle="Save"
                        onRight={()=>Actions.pageOne()}
                         onLeft={()=>Actions.goBack()}
                        editMode 
                        leftTitle="Cancel"
                        tabs={true}
                        navigationBarStyle={{backgroundColor:'blue'}}
                    />
                      <Scene
                        key="homePage"
                        component={Home}
                        title="Home Page"
                        navigationBarStyle={{backgroundColor:'blue'}}
                        hideNavBar
                    /> 
                    <Scene
                        key="donemec"
                        component={DonemecC}
                        title="Donemec"
                        initial
                         hideNavBar
                    />
                    <Scene key='card' component={PageOneC} />
                </Scene>
);

export default () => <RouterWithRedux createReducer={reducerCreate} scenes={Scenes} sceneStyle={{backgroundColor:'#fff'}} />
